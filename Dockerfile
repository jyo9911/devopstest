FROM php:7.1-fpm-alpine3.4
RUN apk update --no-cache \
    && apk add --no-cache 

FROM nginx:1.13.8-alpine 
COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf
WORKDIR ./
COPY app .
